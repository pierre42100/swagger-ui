#!/bin/bash
URL=$(curl https://registry.npmjs.org/swagger-ui-dist | jq . | grep tar | tail -n 1 | cut -d'"' -f 4)
wget -4 "$URL"
tar -xf *.tgz
rm *.tgz
mv package/* .
rmdir package
cp index-overwrite.html index.html
echo "Done"
